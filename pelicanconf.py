#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'erg-type'
SITENAME = u'Le nerf de la lettre'

# Base URL of your website. Not defined by default, so it is best to specify
# your SITEURL; if you do not, feeds will not be generated with properly-formed
# URLs. You should include `http://` and your domain, with no trailing slash at
# the end. Example: `SITEURL = 'http://mydomain.com'`
SITEURL = 'https://erg-type.gitlab.io/le-nerf-de-la-lettre'

# Path to content directory to be processed by Pelican. If undefined, and
# content path is not specified via an argument to the `pelican` command,
# Pelican will use the current working directory.
PATH = 'content'
OUTPUT_PATH = 'public/'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'fr'


THEME = "theme/erg"
CSS_FILE = 'styles.css'


# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 100

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
