Title:
URL:
save_as: index.html

<div class="header">
  <h1><a href="#intro">Le nerf de la lettre</a></h1>
  <a href="#calendrier">Calendrier</a>
  <a href="#groupes">Groupes de travail</a>
</div>


Depuis quelques années, l'option typographie se renforce à l'école et un bon élan se fait ressentir autour du dessin de caractères.
En vue de cerner différentes pratiques contemporaines de recherche en dessin typographique et pour définir avec les étudiants le programme de l'option sur les 3 années à venir, nous organisons en cette fin février 3 journées d'ateliers et de conférences.
Dans cette perspective, les élèves travaillent depuis septembre en petits groupes sur des axes en ligne avec l'intérêt de l'école pour les pratiques expérimentales et les outils libres et pour lesquels ils ont identifié certains acteurs dont une partie seront nos invités.
{: .introduction #intro}


<span id="calendrier"></span>
## erg type days
→ rue du page 87 - 1050 Bruxelles
{: .presentation}

<table>
  <tr>
    <td class="jour">Mercredi 21 février</td>
  </tr>
  <tr>
    <td>Matin : présentations discussions</td>
    <td>10h <a href="http://www.luuse.io/">Luuse</a></td>
    <td>11h <a href="http://writtenrecords.info/">Paul Gangloff</a></td>
    <td>12h <a href="http://typemytype.com/">Frederik Berlaen</a></td>
  </tr>
  <tr>
    <td>Après-midi : ateliers et rencontres</td>
    <td>14h discussion avec Paul Gangloff</td>
    <td>14h workshop avec Frederik Berlaen</td>
  </tr>
</table>

<table>
  <tr>
    <td class="jour">Jeudi 22</td>
  </tr>
  <tr>
    <td>Matin : présentations discussions</td>
    <td>10h <a href="http://baptiste.bernazeau.eu/">Baptiste Bernazeau</a></td>
    <td>11h <a href="http://julienpriezdrawing.tumblr.com/">Julien Priez</a></td>
  </tr>
  <tr>
    <td>14h → 18h : ateliers</td>
    <td>Frederik Berlaen</td>
    <td>Julien Priez</td>
  </tr>
</table>

<table>
  <tr>
    <td class="jour">Vendredi 23</td>
  </tr>
  <tr>
    <td>Matin : présentations discussions</td>
    <td>10h <a href="https://www.speculoos.com/">Pierre Huyghebaert</a></td>
    <td>11h <a href="http://johannesverwoerd.nl/">Johannes Verwoerd</a></td>
  </tr>
  <tr>
    <td>14 → 18h : ateliers </td>
    <td>Frederik Berlaen</td>
    <td>Julien Priez</td>
    <td>Johannes Verwoerd</td>
  </tr>
</table>



<span id="groupes"></span>
## Groupes de recherches

### Ductus folk

Cette recherche s'intéresse à différentes pratiques de lettrages vernaculaires. Elle piste le trait au sein de scènes ou de communautés spécifiques, des tracés en bombe de chantier aux messages en scriptes feutrés. La question de l'outil, du support et du temps est au centre. En observant les influences et hybridations dans le frottement des cultures et de leurs lettrages, le ductus folk tend aussi à montrer les possibles d'une pratique contemporaine du lettrage.
mots clés: graff, modèles d'écritures,
{: .presentation}

En lien :
Filippino Folk Foundry
[François Chastanet : Pixação, Cholo, Dishu](https://vimeo.com/album/1877116),
[Signature, SAEIO](https://vimeo.com/178375812)
{: .liens}

![ductus flok](images/IMG_1264.jpg)
![blackletter](images/blackletter-ductus-plott.jpg)

### Tracé paramétrique
On s'intéresse ici au dessin par son squelette et sa “gesture” digitale. Repartant de Metapost et avec ce qu'on projette sur ce que pourraient être les fontes variables, on se lance sur un chantier de vecteurs avec bagarre d'outils génératifs et extensions sur CNC ou plottées. La typographie au croisement entre écriture et typographie, mieux consciente de son chemin.
{: .presentation}

En lien :
Luuse, Frederik Berlaen
{: .liens}

![meta](images/meta-erg.png)
![trace](images/trace-parametrique-00.jpg)

### Échelle de signe
On piste ici la signalétique identité.
En prenant comme point de départ la signalétique des portes ouvertes de l'erg, cette recherche s'interroge plus largement sur l'installation  du signe à l'échelle d'un événement, d'une manifestation ou d'une ville. Comment un système de signes fait identité en puisant dans un héritage ou une actualité locale ? Comment intégrer les habitants, les occupants d'un lieu à la signalétique créée ?
{: .presentation}

En lien :
Vier5, John Morgan, Karl Nawrot, Eike Köning
{: .liens}


### (Formalsign) Sur le contour
Quand le lettrage devient image.
En présence sur les réseaux et plateforme de partage images, une esthétique nichée de la typographie se fait de plus en plus reconnaître. Avec une influence asiatique et teintée jeux vidéo, les lettrages se déclinent aussi en 3D. Déclinées en affiches, pochettes vinyl, titrages, les influences, le milieu et le plaisir dessinée de cette veine nous amène à revister la question de l'esthétique en typographie.
{: .presentation}

En lien :
Baptiste Bernarzeau, Jonathan Castro, Jacob j wise, guccimaze
{: .liens}


### Ergbats
Établis en consortium, Ergbats s'intéresse aux représentations alternatives de languages et plus particulièrement aux représentations symboliques d'émotions, de concepts, d'objets, de situations, de caractères; tels que les emojis que nous utilisont quotidiennement. L'objectif du consortium est de créer une collection de dingbats propre aux besoins et au contexte particulier de la communauté de l'ERG.
{: .presentation}

En lien :
[Rencontre du troisième type](http://iiitype.anrt-nancy.fr/)
{: .liens}

![fdddl](images/fdddl.jpg)
![treteaux](images/treteaux.jpg)



### Animatype
Le groupe vise à mettre en pratique ce qui modifie le rapport entre l'espace et la typographie. Entre typographie animée, vidéo et réalité augmentée.
{: .presentation}


## Colophon
Typographies : Combined de Julie Patard et Sirius de Laurent Müller.
Disponibles sur la [Typothèque de l'ERG.](http://copyright.rip/erg/typo/)
{: .liens}


<footer></footer>
